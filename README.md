# Ancile Tasks
### About
https://gitlab.com/misc-scripts1/windows/ancile/AncilePlugin_Tasks

Ancile Tasks Allows the blocking of Windows tasks by adding simple configuration files to the data directory.

This is a plugin that requires Ancile_Core (https://gitlab.com/misc-scripts1/windows/ancile/Ancile_Core) 

For more information go to https://voat.co/v/ancile